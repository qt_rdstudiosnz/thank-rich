import './css/reset.css';
import './App.css';
import Navbar from './components/navbar';
import Buttons from './components/buttons'
import { HelmetProvider, Helmet } from 'react-helmet-async';
import { Outlet } from 'react-router-dom';

function App() {
  return (
			<div className="App">
				<HelmetProvider>
					<Helmet>
						<meta charSet="utf-8" />
						<title>Way To Thank Rich Deane & RDStudios 🤙</title>
						<link rel="canonical" href="http://mysite.com/example" />
					</Helmet>
				</HelmetProvider>
				<Navbar />
				<Buttons />
				<Outlet />
			</div>
		);
}

export default App;
