import RDSButtonWrapper from './rdsButton';
import styled from 'styled-components';
import axios from 'axios';
import { useEffect, useState } from 'react';

const ButtonWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;

	& * + * {
		margin-top: 1.6em;
	}
`;

const Buttons = () => {

  const [buttonLinks, setButtonLinks] = useState(false);

  useEffect(()=>{
    const axiosPosts = async () => {
      const response = await axios(
        'https://api.airtable.com/v0/appVcWAtel9r7pVi6/Reviews?maxRecords=50&view=Grid%20view',
        {
          headers: { Authorization: `Bearer ${process.env.REACT_APP_AT_API}` },
        },
      );
      console.log(response.data.records);
      setButtonLinks(response.data.records);
    };
    axiosPosts();
  },[]);
  
  return (
			<ButtonWrapper>
				{buttonLinks &&
					buttonLinks.map((link) => {
						return (
							<RDSButtonWrapper key={link.fields.title} buttonHref={link.fields.link}>
								{link.fields.buttonText}
							</RDSButtonWrapper>
						);
					})}
			</ButtonWrapper>
		);

}

export default Buttons;