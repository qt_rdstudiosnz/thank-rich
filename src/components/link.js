import { useParams } from 'react-router-dom';
import axios from 'axios';
import { useEffect, useState } from 'react';
import styled from 'styled-components';

export default function Link() {

  const [buttonLinks, setButtonLinks] = useState(false);

  useEffect(() => {
    const axiosPosts = async () => {
      const response = await axios(
        'https://api.airtable.com/v0/appVcWAtel9r7pVi6/Reviews?maxRecords=50&view=Grid%20view',
        {
          headers: { Authorization: `Bearer ${process.env.REACT_APP_AT_API}` },
        },
      );
      console.log(response.data.records);
      setButtonLinks(response.data.records);
    };
    axiosPosts();
  }, []);

  let params = useParams();
	const review = params.id;

  let link = "";

  if (buttonLinks) {
    link = buttonLinks.find((link) => link.fields.slug === review);
    window.location.replace(link.fields.link);
  }

	return <LinkDiv>{'Please wait, you are being redirected'}</LinkDiv>;
}

const LinkDiv = styled.div`
	margin-top: 40px;
`;