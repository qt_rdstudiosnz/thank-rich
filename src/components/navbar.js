import logo from '../images/logo.svg';
import styled from 'styled-components';

const Logo = styled.img`
  max-width: 300px;
  width: 90%;
`;

const Wrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;
  padding: 0.8rem;
  margin-bottom: 2.4rem;
`;

const Navbar = () => {
	return (
		<Wrapper>
			<Logo src={logo} alt="Logo" />
		</Wrapper>
	);
};

export default Navbar;