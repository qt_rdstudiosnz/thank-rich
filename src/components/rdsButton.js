import styled from 'styled-components';

const RDSButton = styled.a`
	padding: 1.6rem 2.4rem;
	background-color: rgb(43, 75, 186);
	color: rgb(201, 208, 232);
	font-size: 1.6rem;
	text-align: center;
	border-radius: 0.4rem;
	box-shadow: 2.8px 2.8px 2.2px rgba(0, 0, 0, 0.02),
		6.7px 6.7px 5.3px rgba(0, 0, 0, 0.028),
		12.5px 12.5px 10px rgba(0, 0, 0, 0.035),
		22.3px 22.3px 17.9px rgba(0, 0, 0, 0.042),
		41.8px 41.8px 33.4px rgba(0, 0, 0, 0.05), 100px 100px 80px rgba(0, 0, 0, 0.07);
	transition: 450ms;
	text-decoration: none;

	&:hover {
		transform: scale(1.05);
		background-color: rgba(43, 75, 186, 0.1);
		color: rgba(43, 75, 186, 1);
		box-shadow: 2.8px 2.8px 2.2px rgba(0, 0, 0, 0.019),
			6.7px 6.7px 5.3px rgba(0, 0, 0, 0.035),
			12.5px 12.5px 10px rgba(0, 0, 0, 0.047),
			22.3px 22.3px 17.9px rgba(0, 0, 0, 0.054),
			41.8px 41.8px 33.4px rgba(0, 0, 0, 0.056),
			100px 100px 80px rgba(0, 0, 0, 0.07);
	}
`;

const RDSButtonWrapper = (props) => {
	return <RDSButton href={props.buttonHref} target="_blank">{props.children}</RDSButton>;
};

export default RDSButtonWrapper;
