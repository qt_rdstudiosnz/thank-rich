const buttonLinks = [
	{
		id: '1',
		slug: 'google',
		title: 'Google',
		buttonText: 'Leave Google Review',
		link: 'https://g.page/r/CZ_baMvZ19zQEAI/review',
	},
	{
		id: '2',
		slug: 'facebook',
		title: 'Facebook',
		buttonText: 'Leave Facebook Review',
		link: 'https://www.facebook.com/RDStudiosNZ/reviews/',
	},
	{
		id: '3',
		slug: 'trust Pilot',
		title: 'Trust Pilot',
		buttonText: 'Leave Trust Pilot Review',
		link: 'https://au.trustpilot.com/evaluate/rdstudios.co.nz',
	},
];

export default buttonLinks;