import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Link from './components/link';

ReactDOM.render(
	<React.StrictMode>
		<BrowserRouter>
			<Routes>
				<Route exact path="/" element={<App />}>
				  <Route path=":id" element={<Link />} />
			  </Route>
			</Routes>
		</BrowserRouter>
	</React.StrictMode>,
	document.getElementById('root'),
);


